import 'package:flutter/material.dart';

class DisplayOutput extends StatelessWidget {
  const DisplayOutput({
    super.key,
    required this.text,
  });

  final String text;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      reverse: true,
      child: Row(
        children: <Widget>[
          Container(
            constraints: BoxConstraints(
              minWidth: MediaQuery.of(context)
                  .size
                  .width, // Establece el ancho mínimo deseado
            ),
            padding: const EdgeInsets.all(16),
            child: Text(
              text,
              textAlign: TextAlign.right,
              style: const TextStyle(
                color: Colors.black,
                fontSize: 48,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
