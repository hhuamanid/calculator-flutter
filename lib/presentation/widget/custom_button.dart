import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    super.key,
    required this.label,
    required this.onPressed,
    this.textColor = Colors.black,
    this.backgroundColor = Colors.white,
  });

  final String label;
  final Color textColor;
  final Color backgroundColor;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        shape: const CircleBorder(),
        backgroundColor: backgroundColor,
        padding: const EdgeInsets.all(20),
        elevation: 4,
      ),
      child: Text(
        label,
        style: TextStyle(
          fontSize: 32,
          color: textColor,
        ),
      ),
    );
  }
}
