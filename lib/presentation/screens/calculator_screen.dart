import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

import 'package:cal/presentation/widget/display_input.dart';
import 'package:cal/presentation/widget/display_output.dart';
import 'package:cal/presentation/widget/custom_button.dart';

class CalculatorScreen extends StatefulWidget {
  const CalculatorScreen({super.key});

  @override
  State<CalculatorScreen> createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  String textInput = '';
  String textOutput = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Calculadora'),
        backgroundColor: Colors.white,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            DisplayInput(text: textInput),
            DisplayOutput(text: textOutput),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      if (textInput.isNotEmpty) {
                        textInput =
                            textInput.substring(0, textInput.length - 1);
                      }
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    minimumSize: const Size(156, 12),
                    backgroundColor: Colors.blue,
                    padding: const EdgeInsets.all(20),
                    elevation: 4,
                  ),
                  child: const Text(
                    'Eliminar',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      textInput = '';
                      textOutput = '';
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    minimumSize: const Size(156, 12),
                    backgroundColor: Colors.blue,
                    padding: const EdgeInsets.all(20),
                    elevation: 4,
                  ),
                  child: const Text(
                    'Reiniciar',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomButton(
                  label: '7',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '7';
                    });
                  },
                ),
                CustomButton(
                  label: '8',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '8';
                    });
                  },
                ),
                CustomButton(
                  label: '9',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '9';
                    });
                  },
                ),
                CustomButton(
                  label: '/',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = textOutput;
                        textOutput = '';
                      }
                      textInput += '/';
                    });
                  },
                  backgroundColor: Colors.blue,
                  textColor: Colors.white,
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomButton(
                  label: '4',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '4';
                    });
                  },
                ),
                CustomButton(
                  label: '5',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '5';
                    });
                  },
                ),
                CustomButton(
                  label: '6',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '6';
                    });
                  },
                ),
                CustomButton(
                  label: '*',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = textOutput;
                        textOutput = '';
                      }
                      textInput += '*';
                    });
                  },
                  backgroundColor: Colors.blue,
                  textColor: Colors.white,
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomButton(
                  label: '1',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '1';
                    });
                  },
                ),
                CustomButton(
                  label: '2',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '2';
                    });
                  },
                ),
                CustomButton(
                  label: '3',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '3';
                    });
                  },
                ),
                CustomButton(
                  label: '-',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = textOutput;
                        textOutput = '';
                      }
                      textInput += '-';
                    });
                  },
                  backgroundColor: Colors.blue,
                  textColor: Colors.white,
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomButton(
                  label: '.',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '.';
                    });
                  },
                ),
                CustomButton(
                  label: '0',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = '';
                        textOutput = '';
                      }
                      textInput += '0';
                    });
                  },
                ),
                CustomButton(
                  label: '=',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = textOutput;
                        textOutput = '';
                      } else {
                        try {
                          final expression =
                              (textInput == '') ? '0' : textInput;
                          final parser = Parser();
                          final parsedExpression = parser.parse(expression);
                          final context = ContextModel();
                          final double result = parsedExpression.evaluate(
                            EvaluationType.REAL,
                            context,
                          );
                          textOutput = '$result';
                        } catch (e) {
                          textOutput = 'Error';
                          textInput = '';
                        }
                      }
                    });
                  },
                  backgroundColor: Colors.blue,
                  textColor: Colors.white,
                ),
                CustomButton(
                  label: '+',
                  onPressed: () {
                    setState(() {
                      if (textOutput != '') {
                        textInput = textOutput;
                        textOutput = '';
                      }
                      textInput += '+';
                    });
                  },
                  backgroundColor: Colors.blue,
                  textColor: Colors.white,
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
